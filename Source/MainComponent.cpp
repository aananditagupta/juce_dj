/*
  ==============================================================================

    This file was auto-generated!

  ==============================================================================
*/

#include "MainComponent.h"

//==============================================================================
MainComponent::MainComponent()
{
    // Make sure you set the size of the component after
    // you add any child components.
   
    setSize (800, 600);
    addAndMakeVisible(deck1);
    addAndMakeVisible(deck2);
    addAndMakeVisible(crossFadeSlider);
    addAndMakeVisible(crossFadeLabel);
    
    crossFadeSlider.setRange(-1,1);
    crossFadeSlider.setValue(0.0);
    crossFadeLabel.setText("Cross Fade", dontSendNotification);


    // Some platforms require permissions to open input channels so request that here
    if (RuntimePermissions::isRequired (RuntimePermissions::recordAudio)
        && ! RuntimePermissions::isGranted (RuntimePermissions::recordAudio))
    {
        RuntimePermissions::request (RuntimePermissions::recordAudio,
                                     [&] (bool granted) { if (granted)  setAudioChannels (2, 2); });
    }
    else
    {
        // Specify the number of input and output channels that we want to open
        setAudioChannels (2, 2);
    }
}

MainComponent::~MainComponent()
{
    // This shuts down the audio device and clears the audio source.
    shutdownAudio();
}

//==============================================================================
void MainComponent::prepareToPlay (int samplesPerBlockExpected, double sampleRate)
{
    mixerSource.addInputSource(&player1, false);
    mixerSource.addInputSource(&player2, false);
    mixerSource.prepareToPlay(samplesPerBlockExpected, sampleRate);
    player1.prepareToPlay(samplesPerBlockExpected, sampleRate);
    player2.prepareToPlay(samplesPerBlockExpected, sampleRate);
}

void MainComponent::getNextAudioBlock (const AudioSourceChannelInfo& bufferToFill)
{
    mixerSource.getNextAudioBlock(bufferToFill);
}

void MainComponent::releaseResources()
{
    mixerSource.removeAllInputs();
    mixerSource.releaseResources();
    player1.releaseResources();
    player2.releaseResources();
}
//==============================================================================
void MainComponent::paint (Graphics& g)
{
    
    g.fillAll (getLookAndFeel().findColour (ResizableWindow::backgroundColourId));
}

void MainComponent::resized()
{
    deck1.setBounds(0, 0, getWidth()/3, getHeight());
    deck2.setBounds(getWidth()/3 * 2, 0, getWidth()/3, getHeight());
    crossFadeSlider.setBounds(getWidth()/3, 0, getWidth()/3, getHeight());
    crossFadeLabel.setBounds(getWidth()/3, 20, getWidth()/3, getHeight());
}

void MainComponent::buttonClicked(Button* button)
{
   
}

void MainComponent::sliderValueChanged(Slider* slider)
{
    if(slider == &crossFadeSlider)
    {
        player1.setGain(deck1.volumeSlider.getValue() * (crossFadeSlider.getValue() * -1));
        player2.setGain(deck2.volumeSlider.getValue() * (crossFadeSlider.getValue()));
    }
}
